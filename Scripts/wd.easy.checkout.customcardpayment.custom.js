// PaymentProviderID do vale compra(LoyaltyCard)
var loyaltyCardID = 68; // alterar

function EasyCheckoutCustomCardPaymentCustom(fields, mode, alias) {
	var self = this;
	self.data = {};
	self.validation = ko.observable(false);
	self.errors = ko.observableArray([]);

	var _$parent = null;
	var _memory = {
		currentRequest: null
	};

	self.fields = fields || [];

	var _PREFIX = {
		single: 'PlaceOrder.',
		multi: 'MultiplePlaceOrder.Payments[0].'
	};

	var prefix = _PREFIX.single;

	if (mode === 'b2b') {
		prefix = _PREFIX.multi.replace('Payments[0]', 'Payments[$INDEX]');
	}

	var LoyaltyCardDiscountModel = function(paymentProviderID) {
		var loyaltyCard = _.find(EasyCheckout.ModelData.Basket.LoyaltyCardsList, function(n) {
			return n.PaymentProviderID == paymentProviderID;
		});

		if (!loyaltyCard) {
			loyaltyCard = {
				Amount: 0,
				PaymentProviderID: paymentProviderID,
				DisplayLabel: 'Vale Crédito'
			};
		}

		var LoyaltyCardDiscount = this;

		function fetchBalance() {
			return $.ajax({
					url: browsingContext.Common.Urls.BaseUrl + 'Payment/LoyaltyCard/Balance/' + loyaltyCard.PaymentProviderID,
					type: 'GET',
					cache: true
				})
				.success(function(data) {
					if (data.DisplayLabel) {
						LoyaltyCardDiscount.displayLabel(data.DisplayLabel);
					}

					LoyaltyCardDiscount.balance(parseFloat(data.Balance || 0).toFixed(2));

					ko.postbox.publish('checkout/customcard/custom/vale-compra/fetched', LoyaltyCardDiscount.balance());

					if (!LoyaltyCardDiscount.value()) {
						LoyaltyCardDiscount.inputValue(Math.min(data.Balance || 0, loyaltyCard.Amount));
					}
				});
		}

		function sendData(amount) {
			if (typeof amount === 'number') {
				amount = numeral(amount).format('0,0.00');
			}

			LoyaltyCardDiscount.inputValue(amount);

			_memory.currentRequest = $.ajax({
					url: browsingContext.Common.Urls.Shopping.Basket.Index + '/cartao-fidelidade',
					type: 'POST',
					data: {
						Amount: amount,
						PaymentProviderID: loyaltyCard.PaymentProviderID
					},
					dataType: 'json'
				})
				.done(function(response) {
					$.publish('checkout/response/validate', response, function() {
						if (response.IsValid === false) {
							displayLoyaltyError(response.Errors[0].ErrorMessage);
						} else if (response.Warnings.length) {
							displayLoyaltyError(response.Warnings[0].Message);
						} else {
							cleanLoyaltyErrors();
							// validar necessidade
							ko.postbox.publish('checkout/reload');
							ko.postbox.publish('loylatyCard/reload');

							LoyaltyCardDiscount.visible(false);
							LoyaltyCardDiscount.value(numeral(LoyaltyCardDiscount.inputValue()).value());
						}
					});
				})
				.always(function() {
					_memory.currentRequest = false;
					LoyaltyCardDiscount.isLoading(false);
				});

			return _memory.currentRequest;
		}

		function displayLoyaltyError(msg) {
			LoyaltyCardDiscount.errors.push(msg);
		}

		function cleanLoyaltyErrors() {
			LoyaltyCardDiscount.errors([]);
		}

		function init() {
			fetchBalance();
		}

		LoyaltyCardDiscount.customerIsAuthenticated = EasyCheckout.ModelData.Customer.IsAuthenticated;

		LoyaltyCardDiscount.errors = ko.observableArray([]);

		LoyaltyCardDiscount.isLoyaltyCardEnabled = ko.observable(browsingContext.Common.Config.Payment.IsLoyaltyCardEnabled);
		LoyaltyCardDiscount.balance = ko.observable(0);
		LoyaltyCardDiscount.balanceDecimal = ko.computed(function() {
			return numeral(LoyaltyCardDiscount.balance()).value();
		});

		LoyaltyCardDiscount.displayLabel = ko.observable(loyaltyCard.DisplayLabel);
		LoyaltyCardDiscount.paymentProviderID = ko.observable(loyaltyCard.PaymentProviderID);

		var usedAmount = loyaltyCard.Amount || 0;

		LoyaltyCardDiscount.value = ko.observable(usedAmount || 0);
		LoyaltyCardDiscount.inputValue = ko.observable(0);
		LoyaltyCardDiscount.inputValueDecimal = ko.computed(function() {
			return numeral(LoyaltyCardDiscount.inputValue()).value();
		});
		LoyaltyCardDiscount.isLoading = ko.observable(false);
		LoyaltyCardDiscount.visible = ko.observable(false);

		LoyaltyCardDiscount.formatedValue = ko.computed(function() {
			return LoyaltyCardDiscount.value().formatMoney('.', ',');
		});

		LoyaltyCardDiscount.disabled = ko.computed(function() {
			return numeral(LoyaltyCardDiscount.inputValue()).value() === 0;
		});

		LoyaltyCardDiscount.toggleVisible = function() {
			var visible = LoyaltyCardDiscount.visible();
			if (LoyaltyCardDiscount.value() != 0) {
				LoyaltyCardDiscount.inputValue(LoyaltyCardDiscount.value());
			}
			LoyaltyCardDiscount.visible(!visible);
		};

		LoyaltyCardDiscount.hasSelectedValue = ko.computed(function() {
			return parseFloat(LoyaltyCardDiscount.value()) > 0;
		});

		LoyaltyCardDiscount.useAmount = function() {
			LoyaltyCardDiscount.send();
		};

		LoyaltyCardDiscount.onInputChange = function(model, e) {
			//LoyaltyCardDiscount.send();
			return false;
		};

		LoyaltyCardDiscount.send = function() {
			LoyaltyCardDiscount.isLoading(true);

			var amount = LoyaltyCardDiscount.inputValueDecimal();

			var total = EasyCheckout.ModelData.Basket.Total + LoyaltyCardDiscount.value();
			var loyaltyCardAmountBalance = numeral(LoyaltyCardDiscount.balance()).value();

			if (parseFloat(amount) > loyaltyCardAmountBalance) {
				displayLoyaltyError('Saldo insuficiente');
				LoyaltyCardDiscount.isLoading(false);
			} else if (parseFloat(amount) > total) {
				displayLoyaltyError('Valor utilizado é maior do que o total da compra');
				LoyaltyCardDiscount.isLoading(false);
			} else {
				sendData(amount);
			}
		};

		ko.postbox.subscribe('checkout/data/update', function(data) {

			if (EasyCheckout.ModelData.Basket.LoyaltyCardAmount > 0) {
				LoyaltyCardDiscount.inputValue(LoyaltyCardDiscount.value().formatMoney());
			}

			if (!LoyaltyCardDiscount.customerIsAuthenticated) {
				LoyaltyCardDiscount.customerIsAuthenticated = EasyCheckout.ModelData.Customer.IsAuthenticated;
				init();
			}
		});

		init();
	};

	var loadMetadata = function(prefix) {
		self.metadata = new MetadataKo();

		self.metadata.init(self.fields, self.data, prefix);

		self.metadata.fields.map(function(field) {
			field.attr = {
				EntityMetadataID: false,
				Name: false,
				Value: _(field.attr.Value).extend({
					name: [prefix, "Custom", '["', field.name, '"]'].join(''),
					id: [prefix.replace(/\[.*\]|\W/gi, ''), field.name].join('_'),
					value: field.value,
					maxlength: field.maxLength,
					placeholder: field.hint
				}),
				InputMask: '9999'
			};

			if (field.type === 'Email') {
				self.data[field.name].extend({
					email: {
						message: 'Email é inválido.'
					}
				});
			}

			if (field.alias === 'numbercard') {
				field.mask = '99999999';
			}

			if (field.alias === 'paymentdue') {
				field.mask = '99/99/9999';
			}

			return field;
		});

	};

	loadMetadata(prefix);

	self.useDownPaymentMethod = ko.observable(true);

	self.toggleDownPaymentMethods = function($parent) {
		// console.log('$parent', ko.toJS($parent));
		var newVal = !self.useDownPaymentMethod();

		self.useDownPaymentMethod(newVal);
		$parent.useDownPaymentMethod(newVal);

		_$parent = $parent;
	};

	ko.postbox.subscribe('checkout/customcard/custom/useDownpayment', function(val) {
		self.useDownPaymentMethod(val);
		_$parent.useDownPaymentMethod(val);
	});

	self.selectedDownPaymentMethod = ko.observable(0).extend({
		required: {
			message: 'Selecione uma forma de entrada',
			onlyIf: function() {
				return _$parent && _$parent.useDownPaymentMethod();
			}
		},
		validation: {
			validator: function(val) {
				return typeof val === 'object';
			},
			onlyIf: function() {
				return _$parent && _$parent.useDownPaymentMethod();
			},
			message: 'Selecione uma forma de entrada'
		}
	});
	self.selectedDownPaymentMethod.disableSuccess = true;
	self.selectedDownPaymentMethodID = ko.computed(function() {
		var method = self.selectedDownPaymentMethod();

		if (_$parent) {
			_$parent.selectedDownPaymentMethod(method);
		};

		return method ? ko.unwrap(method.PaymentMethodID) : null;
	});

	self.downPaymentAmount = ko.observable().extend({
		required: {
			message: 'O valor deve ser superior a R$ 0,00',
			onlyIf: function() {
				return _$parent && _$parent.useDownPaymentMethod();
			}
		},
		validation: {
			validator: function(val) {
				val = numeral(val).value();
				if (!_$parent) {
					return false;
				}

				var settings = _$parent.card.downPaymentSettings();

				if (!settings.isEnabled) {
					return true;
				}

				if (settings.maxDownPaymentAmount > 0 &&
					val > settings.maxDownPaymentAmount
				) {
					return false;
				}

				if (settings.minDownPaymentAmount > 0 &&
					val < settings.minDownPaymentAmount
				) {
					return false;
				}

				return true;
			},
			onlyIf: function() {
				return _$parent && _$parent.useDownPaymentMethod();
			},
			message: 'Valor não permitido'
		}
	});
	self.downPaymentAmount.disableSuccess = true;
	self.downPaymentAmountDecimal = ko.computed(function() {
		return numeral(self.downPaymentAmount()).value() || 0;
	});

	self.errors = ko.validation.group(self.data);
	self.dawnPaymentErrors = ko.validation.group(self);

	self.computeTotalAmount = function($parent, format) {
		var val = ($parent.card.getTotalPayable() || 0) - self.downPaymentAmountDecimal();

		return format ? numeral(val).format('0,0.00') : val;
	};

	// custom - vale-credito
	self.useLoyaltyCardAsDownPayment = ko.observable(false);
	// self.loyaltyCard = ko.observable(new LoyaltyCardDiscountModel(loyaltyCardID));
	self.loyaltyCard = new LoyaltyCardDiscountModel(loyaltyCardID);

	function isTotalPaidWithLoyaltyCard() {
		return EasyCheckout.ModelData.Basket.LoyaltyCardPaymentAmount === EasyCheckout.ModelData.Basket.Total;
	}

	ko.postbox.subscribe('checkout/data/update', function() {
		// self.loyaltyCard(LoyaltyCardDiscountModel(loyaltyCardID));
		self.loyaltyCard = new LoyaltyCardDiscountModel(loyaltyCardID);
	});

	// Evento de validação do formulário CUSTOMCARD é o nome do metodo de pagamento selecionado
	$.subscribe('EasyCheckout/submit/validation/CUSTOMCARD/' + alias, function(e, promise) {
		var p = promise();
		self.errors.showAllMessages();
		self.dawnPaymentErrors.showAllMessages();
		var errors = self.errors();
		var downPaymenterrors = self.dawnPaymentErrors();
		var useDownPaymentMethod = self.useDownPaymentMethod();

		if (isTotalPaidWithLoyaltyCard()) {
			p.resolve();
		}

		if (errors.length === 0 && downPaymenterrors.length === 0) {

			if (useDownPaymentMethod &&
				(self.downPaymentAmountDecimal() <= 0 || !self.selectedDownPaymentMethodID())
			) {
				p.reject(['Escolha uma forma de entrada']);
			}

			p.resolve();
		} else {
			p.reject(errors);
		}
		self.validation(true);
	});

	// Handler do submit do easy após validações, no final do processo deve ser chamado o método _submit
	window['customcard_' + alias + '_doPayment'] = function(method, data, _submit) {
		if (_memory.currentRequest) {
			ko.postbox.publish('scene/add', 'loading');

			_memory.currentRequest
				.complete(function() {
					window['customcard_' + alias + '_doPayment'](method, data, _submit);
				});

			return;
		}

		if (isTotalPaidWithLoyaltyCard()) {
			return _submit(data);
		}

		if (!ko.unwrap(self.useDownPaymentMethod)) {
			return _submit(data);
		}

		_submit(_(data).map(function(d) {
			if (!!~d.name.indexOf('MultiplePlaceOrder')) {
				return d;
			}

			return {
				name: d.name.replace(_PREFIX.single, _PREFIX.multi),
				value: d.value
			};
		}));
	};

	ko.postbox.publish('checkout/payment/bemol/loaded', {});
};

// EasyCheckoutCustomCardPaymentCustom.prototype.dispose = function() {
// };

(function($, window, document, undefined) {
	$.widget('wd.EasyCheckoutCustomcardpaymentCustom', $.wd.widget, {
		// Private
		_create: function() {
			var $widget = this,
				$w = $widget.getContext(),
				alias = $w().data('payment-alias');

			if (!alias) {
				console.error('É necessário passar o parâmetro "PaymentMethodAlias" para o widget easy.checkout.customcardpayment');
				return;
			}

			$widget._registerKoComponent(alias);
		},
		_registerKoComponent: function(alias) {
			ko.components.register('easy_checkout_customcardpayment_' + alias, {
				template: {
					element: 'easy_checkout_customcardpayment_' + alias
				},
				viewModel: function(params) {
					var cacheKey = 'customCardMemory_' + alias;
					var cacheObject = null;

					if (window[cacheKey]) {
						cacheObject = window[cacheKey];
					} else {
						cacheObject = new EasyCheckoutCustomCardPaymentCustom(params.fields, params.mode, alias);
						ko.postbox.publish('easy-checkout-customcardpayment-custom/cached');
					}

					window[cacheKey] = cacheObject;

					_.extend(this, cacheObject);

					ko.postbox.publish('easy-checkout-customcardpayment-custom/created');
				},
				synchronous: true
			});
		}
	});
})(jQuery, window, document);

// Lógica personalizada para ações macro
$(function() {

	function checkCustomCard(hash) {
		var action = 'hide';

		var loyaltyCard = _.find(EasyCheckout.ModelData.Basket.LoyaltyCardsList, function(n) {
			return n.PaymentProviderID == loyaltyCardID;
		});

		if (hash.indexOf('payment/customcard') == -1) {
			action = 'show';
		} else {
			if (loyaltyCard) {
				ko.postbox.publish('checkout/loyaltycard/update/' + loyaltyCardID, 0);
			}
		}

		$('[data-loyaltycard-id="' + loyaltyCardID + '"]')[action]();

	}

	function subscribeHashChange() {
		window.addEventListener('hashchange', function() {
			checkCustomCard(window.location.hash);
		}, false);

		checkCustomCard(window.location.hash);
	}

	function forceToggle() {
		setTimeout(function() {
			var $toggable = $('[data-toggle-downpayment].opened');
			if ($toggable.length) {
				$toggable
					.trigger('click')
					.trigger('click');
			}
		}, 90);
	}

	function init() {
		subscribeHashChange();

		/*
		$.subscribe('checkout/errors/push', function() {
			var loyaltyCard = _.find(EasyCheckout.ModelData.Basket.LoyaltyCardsList, function(n) {
				return n.PaymentProviderID == loyaltyCardID;
			});

			if (loyaltyCard) {
				setTimeout(function() {
					ko.postbox.publish('checkout/loyaltycard/update/' + loyaltyCardID, 0);
				}, 90);
			}
		});
		*/

		// workaround para atualizar $_parent dentro do escopo da view model do component
		ko.postbox.subscribe('checkout/data/update', function(data) {
			forceToggle();
		});

		ko.postbox.subscribe('easy-checkout-customcardpayment-custom/created', function() {
			forceToggle();
		});
	}

	init();
});
